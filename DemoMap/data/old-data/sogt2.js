﻿$(function () {

    // Create the chart
    var chart = new Highcharts.chart('container1', {
        chart: {
            type: 'column',
            events: {
                load: function () {
                    var count = 0;
                    setInterval(function () {
                        if (count == 0) {
                            chart.series[0].setData([15, 44]);
                            //chart.series[1].setData([10.57]);
                            //chart.series[2].setData([7.23]);
                            count = 1;
                        }
                        else {
                            chart.series[0].setData([0, 0]);
                            //chart.series[1].setData([0]);
                            //chart.series[2].setData([0]);
                            count = 0;
                        }
                    }, 2000);
                }
            }
        },
        title: {
            text: 'Biểu đồ tỷ lệ TNGT nghiêm trọng so với cùng kỳ năm ngoái',
            style: {
                color: '#000000',
                //fontWeight: 'bold'
            }
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['số người chết', 'số người bị thương'],
            
        },
        colors: ['#cddc39', '#ffc107'],
        yAxis: {
            max:50,
            title: {
                text: '%'
            }
        },
        //colors: ['green', 'red', 'blue'],

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: { enabled: false },
        tooltip: {
            headerFormat: '<span style="font-size:11px"></span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
        },

        "series": [
          {
              //"name": "Browsers",
              "colorByPoint": true,
              "data": [
                {
                    "name": "số người chết",
                    "y": 15,
                },
                {
                    "name": "số người bị thương",
                    "y": 44,
                }
              ]
          }
        ],
        
    });
    
});