﻿$(function () {

    // Create the chart
    var chart = new Highcharts.chart('container', {
        chart: {
            type: 'column',
            events: {
                load: function () {
                    var count = 0;
                    setInterval(function () {
                        if (count == 0) {
                            chart.series[0].setData([79, 85, 122, 117, 103, 103]);
                            //chart.series[1].setData([10.57]);
                            //chart.series[2].setData([7.23]);
                            count = 1;
                        }
                        else {
                            chart.series[0].setData([0, 0, 0, 0, 0, 0]);
                            //chart.series[1].setData([0]);
                            //chart.series[2].setData([0]);
                            count = 0;
                        }
                    }, 2000);
                }
            }
        },
        title: {
            text: 'Biểu đồ kết quả thực hiện các biện pháp tránh thai mới theo chỉ tiêu năm',
            style: {
                color: '#000000',
                //fontWeight: 'bold'
            }
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['Đình sản', 'DCTC', 'Thuốc cấy', 'Thuốc tiêm', 'Thuốc uống', 'BCS'],
            
            labels: {
                autoRotation: 0,
                
            }
        },
        yAxis: {
            max:123,
            title: {
                text: '%'
            }
        },
        //colors: ['green', 'red', 'blue'],

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },
        credits: {
            enabled: false
        },
        exporting: { enabled: false },
        tooltip: {
            headerFormat: '<span style="font-size:11px"></span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
        },

        "series": [
          {
              //"name": "Browsers",
              "colorByPoint": true,
              "data": [
                {
                    "name": "Đình sản",
                    "y": 79,
                },
                {
                    "name": "DCTC",
                    "y": 85,
                },
                {
                    "name": "Thuốc cấy",
                    "y": 122,
                },
                {
                    "name": "Thuốc tiêm",
                    "y": 117,
                },
                {
                    "name": "Thuốc uống",
                    "y": 103,
                },
                {
                    "name": "BCS",
                    "y": 103,
                }
              ]
          }
        ],
        
    });
    
});