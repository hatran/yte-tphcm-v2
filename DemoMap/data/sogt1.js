﻿$(function () {

    // Create the chart
    var chart = new Highcharts.chart('container', {
        chart: {
            type: 'column',
            //events: {
            //    load: function () {
            //        var count = 0;
            //        setInterval(function () {
            //            if (count == 0) {
            //                chart.series[0].setData([44, 8]);
            //                //chart.series[1].setData([10.57]);
            //                //chart.series[2].setData([7.23]);
            //                count = 1;
            //            }
            //            else {
            //                chart.series[0].setData([0, 0]);
            //                //chart.series[1].setData([0]);
            //                //chart.series[2].setData([0]);
            //                count = 0;
            //            }
            //        }, 1700);
            //    }
            //}
        },
        title: {
            text: 'Biểu đồ thanh tra GTVT và Trạm kiểm tra tải trọng xe lưu động',
            style: {
                color: '#000000',
                //fontWeight: 'bold'
            }
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['Lập biên bản VPHC và ra quyết định xử phạt vi phạm hành chính ', 'Đình chỉ và yêu cầu khắc phục hậu quả do vi phạm hành chính gây ra '],
            
            labels: {
                autoRotation: 0,
                
            }
        },
        yAxis: {
            max:50,
            title: {
                text: '%'
            }
        },
        //colors: ['green', 'red', 'blue'],

        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },
        credits: {
            enabled: false
        },
        colors: ['#cddc39', '#ffc107'],
        exporting: { enabled: false },
        tooltip: {
            headerFormat: '<span style="font-size:11px"></span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
        },

        "series": [
          {
              //"name": "Browsers",
              "colorByPoint": true,
              "data": [
                {
                    "name": "Lập biên bản VPHC và ra quyết định xử phạt vi phạm hành chính ",
                    "y": 44,
                },
                {
                    "name": "Đình chỉ và yêu cầu khắc phục hậu quả do vi phạm hành chính gây ra ",
                    "y": 8,
                }
              ]
          }
        ],
        
    });
    
});