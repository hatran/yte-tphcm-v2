var mook = {
    sobn: function () {
        let raw = `
        Bệnh viện Điều dưỡng - Phục hồi chức năng TP HỒ CHÍ MINH
        Trung tâm chăm sóc sức khỏe sinh sản
        Bệnh viện Đa khoa Cao su Dầu Tiếng
        Trung tâm Y tế thành phố Thủ Dầu Một
        Bệnh viện Đa khoa huyện Dĩ An
        Trung tâm Y tế thị xã Thuận An
        Trung tâm Y tế thị xã Bến Cát`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `
        Bệnh viện Điều dưỡng - Phục hồi chức năng TP HỒ CHÍ MINH
        Trung tâm chăm sóc sức khỏe sinh sản
        Bệnh viện Đa khoa Cao su Dầu Tiếng
        Trung tâm Y tế thành phố Thủ Dầu Một
        Bệnh viện Đa khoa huyện Dĩ An
        Trung tâm Y tế thị xã Thuận An
        Trung tâm Y tế thị xã Bến Cát`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = `Quận 1
                    Quận 12
                Quận Thủ Đức
                Quận 9
                    Quận Gò Vấp
                Quận Bình Thạnh
                    Quận Tân Bình
                    Quận Tân Phú
                    Quận Phú Nhuận`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `
        Bệnh viện Điều dưỡng - Phục hồi chức năng TP HỒ CHÍ MINH
        Trung tâm chăm sóc sức khỏe sinh sản
        Bệnh viện Đa khoa Cao su Dầu Tiếng
        Trung tâm Y tế thành phố Thủ Dầu Một
        Bệnh viện Đa khoa huyện Dĩ An
        Trung tâm Y tế thị xã Thuận An
        Trung tâm Y tế thị xã Bến Cát`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = `
        Bệnh viện Điều dưỡng - Phục hồi chức năng TP HỒ CHÍ MINH
        Trung tâm chăm sóc sức khỏe sinh sản
        Bệnh viện Đa khoa Cao su Dầu Tiếng
        Trung tâm Y tế thành phố Thủ Dầu Một
        Bệnh viện Đa khoa huyện Dĩ An
        Trung tâm Y tế thị xã Thuận An
        Trung tâm Y tế thị xã Bến Cát`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}
